export class modeleCotation {

    num:number; //NUMERO DE MODELe
    noteListeGrise:number; 
    noteListeNoire:number; 
    notePaysCorruption:number; 
    notePaysEconomieParallele:number; 
    notePaysGuerre:number; 
    notePaysDrogues:number; 
    noteSecteur:number; 
    noteProfession:number; 
    notePepPersonnePhysique:number; 
    notePepPersonneMorale:number; 



    //PersonneMorale
    noteNature:number; 
    noteParticipationPub:number; 
    noteOffshore:number; 
    notePaysOrigineFonds:number; 
    noteOrigineFonds :number; 
    noteMarcheBoursier:number; 
    noteAssOffshore:number; 
    ong:number; 
    noteChiffreAffaire:number; 
    noteBlackListePP:number; 

    //PersonnePhysique
    residence:number; 
    intermediaire:number; 
    noteBlackListePM:number; 
}