import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TratiterOperationComponent } from './tratiter-operation.component';

describe('TratiterOperationComponent', () => {
  let component: TratiterOperationComponent;
  let fixture: ComponentFixture<TratiterOperationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TratiterOperationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TratiterOperationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
